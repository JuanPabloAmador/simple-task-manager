## Simple Task Manager

**Puedes encontrar la app funcionando en el siguiente enlace: [http://simpletaskmanager.juanpabload.com]( http://simpletaskmanager.juanpabload.com)/**

Es una app web creada con **React JS** que simula ser una app de gestión de tareas.
Se utilizó **Bootstrap** para la maquetación y el diseño responsive.

La aplicación cuenta con 3 componentes: 

1. La navegación, que se utiliza tanto en el header como en el footer.
2. El formulario, que almacena en su estado los valores de los inputs del mismo y envía al tercer componente.
3. App.js, el componente que se encarga de manejar toda la app, crea dinamicamente las cards de bootrap con 
la información suministrada por el formulario.

Un especial agradecimiento a Fazt del canal de [Youtube](https://www.youtube.com/channel/UCX9NJ471o7Wie1DQe94RVIg) 
autor intelectual de este ejemplo. 